package com.agustin.challenge;

import static org.junit.Assert.assertTrue;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import com.agustin.challenge.managers.ScorePrinter;
import com.agustin.challenge.model.ScoreFrame;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ScorePrinterTests {

	private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
	private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
	private final PrintStream originalOut = System.out;
	private final PrintStream originalErr = System.err;

	@Before
	public void setUpStreams() {
		System.setOut(new PrintStream(outContent));
		System.setErr(new PrintStream(errContent));
	}
	
	@After
	public void restoreStreams() {
		System.setOut(originalOut);
		System.setErr(originalErr);
	}

	@Test
	public void prints() { //unit test
		ArrayList<ScoreFrame> scores = new ArrayList<>(10);
		ScoreFrame sc;
		for (int i = 0; i<10 ; i++) {
			sc = new ScoreFrame();
			sc.setFirst(10); sc.setTotal(30);

			if (i == 9) {
				sc.setLast(10); sc.setSecond(10);
			}

			scores.add(sc);
		}
		ScorePrinter sp = new ScorePrinter("Maggie", scores);
		sp.print();
		String s = outContent.toString().trim().replace("\r", "");
		String r = "Maggie\nPinfalls\t\tX\t\tX\t\tX\t\tX\t\tX\t\tX\t\tX\t\tX\t\tX\tX\tX\tX\nScore\t\t30\t\t60\t\t90\t\t120\t\t150\t\t180\t\t210\t\t240\t\t270\t\t300";
		boolean b = s.matches(r);
		assertTrue(b);
	}

}
