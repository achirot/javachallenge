package com.agustin.challenge;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.HashMap;

import com.agustin.challenge.managers.GameManager;
import com.agustin.challenge.managers.ScoreBoard;
import com.agustin.challenge.model.Player;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ScoreBoardTests {

	private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
	private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
	private final PrintStream originalOut = System.out;
	private final PrintStream originalErr = System.err;

	@Before
	public void setUpStreams() {
		System.setOut(new PrintStream(outContent));
		System.setErr(new PrintStream(errContent));
	}
	
	@After
	public void restoreStreams() {
		System.setOut(originalOut);
		System.setErr(originalErr);
	}

	@Test
	public void prints() { //unit test
		HashMap<String,Player> players = new HashMap<>();
		Player krusty = new Player("Krusty");

		for (int i = 0; i<12 ; i++) krusty.addResult(10);

		players.put("Krusty", krusty);
		GameManager gm = new GameManager();
		gm.setPlayers(players);

		ScoreBoard sb = new ScoreBoard(gm);
		try {sb.printScores();} catch(Exception e) { fail(e.getMessage()); }

		String s = outContent.toString().trim().replace("\r", "");
		String r = "Frame\t\t1\t\t2\t\t3\t\t4\t\t5\t\t6\t\t7\t\t8\t\t9\t\t10\nKrusty\nPinfalls\t\tX\t\tX\t\tX\t\tX\t\tX\t\tX\t\tX\t\tX\t\tX\tX\tX\tX\nScore\t\t30\t\t60\t\t90\t\t120\t\t150\t\t180\t\t210\t\t240\t\t270\t\t300";
		boolean b = s.matches(r);
		assertTrue(b);
	}

}
