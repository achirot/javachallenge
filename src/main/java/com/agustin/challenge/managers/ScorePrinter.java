package com.agustin.challenge.managers;

import java.util.List;

import com.agustin.challenge.model.ScoreFrame;

/** prints scores */
public class ScorePrinter {
    String player;
    List<ScoreFrame> scores;

    /** Creates a new Printer for a player name and his/her scores
     * then the print method can be used.
     * @param player name
     * @param scores the list of the scores in frames of the player
    */
    public ScorePrinter(String player, List<ScoreFrame> scores) {
        this.player = player;
        this.scores = scores;
    }

    /** prints the associated score*/
    public void print() {
        System.out.println("\n"+player);
        this.printPinfalls();
        this.printScores();
    }
    
    private void printScores() {
        int acum = 0, i = 0;
        System.out.print("Score\t\t");
        for (ScoreFrame frame : scores) { 
            acum += frame.getTotal();
            if (i++ < 9)
                System.out.printf("%d\t\t",acum);
            else
                System.out.printf("%d",acum);
        }
    }

    private void printPinfalls() {
        System.out.print("Pinfalls\t");
        int index=0;

        for (ScoreFrame frame : scores) {

            if ( index++ < 9) 
                printRegularPinfall(frame);
            else
                printLastPinfall(frame);
        }
    }

    private void printRegularPinfall(ScoreFrame frame) {
        Integer x = frame.getFirst(), y = frame.getSecond();

        if (x != null) {
            switch (x) {
                case 10: System.out.print("\tX\t"); break;
                case -1: System.out.print("F\t"); break;
                default: System.out.print(x+"\t"); break;
            }
        }

        if (y != null) {
            if (x + y == 10)
                System.out.print("/\t");
            else if (y == -1)
                System.out.print("F\t");
            else
                System.out.print(y + "\t");
        }

    }

    private void printLastPinfall(ScoreFrame frame) {
        Integer x = frame.getFirst(),
                y = frame.getSecond(),
                z = frame.getLast();

        if (x != null) {
            switch (x) {
                case 10: System.out.print("X\t"); break;
                case -1: System.out.print("F\t"); break;
                default: System.out.print(x+"\t"); break;
            }
        }

        if (y != null) {
            if (x == 10) {
                switch (y) {
                    case 10: System.out.print("X\t"); break;//double
                    case -1: System.out.print("F\t"); break;
                    default: System.out.print(y+"\t"); break;
                }
            }
            else {
                if (x+y==10) 
                    System.out.print("/\t");
                else if (y == -1)
                    System.out.print("F\n");
                else 
                    System.out.print(y+"\n");
            }
        }

        if (z != null) {
            if (z==10 && y == 10 && x==10 ) 
                System.out.print("X\n"); //triple
            else if (x+y==10) {
                if (z == 10) 
                    System.out.print("X\n");
                else if (z == -1)
                    System.out.print("F\n");
                else 
                    System.out.print(z+"\n");
            }
            else if (z+y==10)
                System.out.print("/\n");
            else if (z == -1)
                System.out.print("F\n");
            else
                System.out.print(z+"\n");
        }

    }

}