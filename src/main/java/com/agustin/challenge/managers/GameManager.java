package com.agustin.challenge.managers;

import java.io.File;
import java.util.HashMap;
import java.util.Scanner;

import com.agustin.challenge.model.Player;
import com.agustin.challenge.util.GameException;

/** A game manager controlls a game and all in it */
public class GameManager {
    HashMap<String,Player> players;

    public GameManager() {
        players = new HashMap<>();
    }

    /**
     * @return the players
     */
    public HashMap<String, Player> getPlayers() {
        return players;
    }

    /**
     * Set the players of this game
     * @param players a map for (name,player)
     */
    public void setPlayers(HashMap<String, Player> players) {
        this.players = players;
    }

    /** 
     * Starts a game by reading the input, and showing the score at the end 
     * @param filename the full name of the file of results to be loaded
    */
    public void start(String filename) {
        Scanner input = null;

        try {
            File file = new File(filename);
            input = new Scanner(file).useDelimiter("\\s+"); //space

            while (input.hasNextLine()) {
                String dude = input.next(), res = input.next();

                if (! isValidName(dude) )
                    throw new GameException("Invalid Player Name, must be only letters");

                int round = parseRoundResult(res);
                    
                //if (input.hasNextLine())
                //    input.nextLine();

                this.addRound(dude,round);
            }
            new ScoreBoard(this).printScores();

        } catch (Exception ex) {
            String msg = ex.getMessage();
            System.out.println("\nError: " + ((msg == null) ? ex.toString(): msg) + "\n");
            //ex.printStackTrace();
        } finally {
            if (input != null) input.close();
        }
    }

    /** adds a round to the game
     * @param player the dude/girl
     * @param result the result of throwing the ball
    */
    private void addRound(String player, int result) {
        if (!players.containsKey(player))
            players.put(player, new Player(player));

        players.get(player).addResult(result);
    }


    /** Parses the result of a ball throw
     * @param result the result number or F for fault
     * @return a result value between 0 and 10, or -1 for fault
     * @throws Exception with a parse fail message
     */
    private int parseRoundResult(String result) throws Exception {
        if ("F".equals(result))
            return -1;
        else
            try {
                int r = new Integer(result);
                if (r < 0 || r > 10) {
                    throw new GameException("the throw result is out of (0,10)+F Range");
                }
                return r;
            } 
            catch (NumberFormatException e) {
                throw new GameException("the throw result is not either a number nor a Fault");
            }
    }

    /** validates proper player name 
     * @param name of the player
     * @return true if valid
    */
    private boolean isValidName(String name) {
        return name.matches("^[a-zA-z]+$");
    }

}