package com.agustin.challenge.managers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.agustin.challenge.model.ScoreFrame;
import com.agustin.challenge.util.GameException;

/** Holds scores */
public class ScoreBoard {
    final GameManager game;
    final HashMap<String,List<ScoreFrame>> scores;

    /**
     * Creates a ScoreBoard for a game
     * @param game the game manager
     */
    public ScoreBoard(GameManager game) {
        this.game = game;
        scores = new HashMap<>();
        game.players.keySet().forEach( player -> {
            ArrayList<ScoreFrame> list = new ArrayList<>(10);
            for (int i = 0; i<10 ; i++)
                list.add(new ScoreFrame());

            scores.put(player, list);
        });
    }

    /** prints the results of the game 
     * @throws GameException usually when the game rules are not being followed
    */
    public void printScores() throws GameException {

        if (game.players.isEmpty())
            throw new GameException("There are no players");

        //per player
        for (String player : game.players.keySet()) {
            this.loadScores(player);
            if (!areValidScores(player))
                throw new GameException("Invalid game, scores are not valid");
        }

        //printing - note: we are looping twice for validation
        printBoardHeader();
        game.players.keySet().forEach( player -> { new ScorePrinter(player, scores.get(player)).print(); });
    }

    /** prints the ScoreBoard header */
    private void printBoardHeader() {
        //System.out.println("----------\r\nScoreBoard\r\n----------");
        System.out.print("Frame\t\t");
        for(int i = 1; i<10; i++)
            System.out.printf("%d\t\t",i);
        System.out.print("10");    
    }

    /** 
     * Validates a current set of scores belongs to a real game
     * @param player name
     * @return true if valid */
    private boolean areValidScores(String player) {

        List<ScoreFrame> frames = scores.get(player);

        for (int i = 0; i<9; i++) {
            ScoreFrame frame = frames.get(i);
            if (frame.getFirst() == null || (frame.getFirst() != 10 && frame.getSecond() == null))
                return false;
        }
        ScoreFrame frame = frames.get(9);
        if (frame.getFirst() == null || frame.getSecond() == null
            || ((frame.getSecond() == 10 
                || (frame.getSecond() + frame.getFirst()  == 10))
                && frame.getLast() == null))
            return false;
        
        return true;
    }


    /** set player results in organized frames
     * @param player the player's name
     * @throws GameException usually when the game rules are not being followed
     */
    private void loadScores(String player) throws GameException{
        ArrayList<Integer> results = game.players.get(player).getResults();
        int frameIdx = 0, len = results.size();

        for (int i=0; i<len; i++) {
            if (frameIdx > 9) 
                throw new GameException("there are too many rounds!");

            ScoreFrame frame = scores.get(player).get(frameIdx);
            int result = results.get(i);

            if (frame.getFirst() == null) {
                frame.setFirst(result);
                if (result == 10)  { //strike
                    if (i+2 >= len)
                        throw new GameException("there are too few elements");
                    frame.setTotal( 10 
                        + Math.max( results.get(i+1) , 0)
                        + Math.max( results.get(i+2) , 0));
                    if (frameIdx < 9)
                        frameIdx++;
                } 
            }

            else if(frame.getSecond() == null) {
                frame.setSecond(result);

                int first = Math.max(frame.getFirst(),0);

                if (result + first == 10) {
                    if (i+1 >= len)
                        throw new GameException("there are too few elements");
                    frame.setTotal(10 + Math.max(results.get(i+1),0));
                } else {
                    if ( (first + Math.max(result,0) > 10 && frameIdx < 9)
                        || (first + Math.max(result,0) > 20 && frameIdx == 9) )
                        throw new GameException("cannot take down more than 20 pins");

                    frame.setTotal(first + Math.max(result,0));
                }
                if (frameIdx < 9)
                    frameIdx++;
            }

            else if (frameIdx == 9 && frame.getLast() == null) { //last one
                frame.setLast(result);
                frame.setTotal(frame.getTotal() + Math.max(result,0)); //extra shot
                if (frame.getTotal() > 30) 
                    throw new GameException("cannot take score more than 30");
                frameIdx++;
            }
            
        }
    }
}