package com.agustin.challenge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.agustin.challenge.managers.GameManager;

import org.springframework.boot.Banner;
/** Main class for this project, boots games */
@SpringBootApplication
public class JavaChallengeApplication {
	
	/**
	 * Starts the Application given a filename
	 * @param args a list of args where the first one is the file name with the game results
	 */
	public static void main(String[] args) {

		SpringApplication app = new SpringApplication(JavaChallengeApplication.class);
        app.setBannerMode(Banner.Mode.OFF);
		app.run(args);
		
		if (args[0] != null)
			new GameManager().start(args[0]);
		else
			System.out.println("\nSe requiere un nombre de archivo\n");
	}

}
