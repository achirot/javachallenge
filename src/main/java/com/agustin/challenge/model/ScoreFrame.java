package com.agustin.challenge.model;

/**
 * represent a player individual score in a frame, that is
 * a group of first, second and maybe a third (last) throw
 */
public class ScoreFrame {
    int total=0;
    Integer first=null, second=null, last=null;

    /**
     * @return the first throw result of this frame
     */
    public Integer getFirst() {
        return first;
    }

    /**
     * Set the first throw result
     * @param first the amount of pins that has fallen
     */
    public void setFirst(Integer first) {
        this.first = first;
    }

    /**
     * @return the second throw result of this frame
     */
    public Integer getSecond() {
        return second;
    }

    /**
     * Set the second throw result
     * @param second the amount of pins that has fallen
     */
    public void setSecond(Integer second) {
        this.second = second;
    }

    /**
     * @return the total score made in this frame
     */
    public int getTotal() {
        return total;
    }

    /** Sets the total score made in this frame
     * @param total score 
     */
    public void setTotal(int total) {
        this.total = total;
    }

    /**
     * @return the last throw result of this frame
    */
    public Integer getLast() {
        return last;
    }

    /**
     * Set the last throw result
     * @param last the amount of pins that has fallen
     */
    public void setLast(Integer last) {
        this.last = last;
    }
}