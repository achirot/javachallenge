package com.agustin.challenge.model;

import java.util.ArrayList;

/** represents a player */
public class Player {
    String name;
    ArrayList<Integer> results = new ArrayList<>();

    /**
     * makes a new player
     * @param name the name of the player
     */
    public Player(String name) {
        this.name = name;
    }

    /**
     * gets the name of this player
     * @return player's name
     */
    public String getName() { return name; }

    /**
     * adds a new result of fallen pins, after player action,
     * result should be between 0 and 10, or -1 for a fault
     * @param result the new throw fallen pins or -1 for fault
     */
    public void addResult(int result) {
        results.add(result);
    }
    
    /**
     * Gets all results of this player
     * @return a list of results
     */
    public ArrayList<Integer> getResults() {
        return results;
    }

}