# Java Challenge

This projects contains a solution thats reads the raw results of a 10-pin bowling game and returns the compleated scoreboard.
> note: this projects uses Springboot

## Compile

This project is based of a wrapped Maven build system, java 8+ must be installed, the run

    mvn package

## Run

The resulting jar file can be run by having an input file an doing:

    java -jar /target/challenge-1.0.jar <filename>.<extension>

for example

    java -jar target/challenge-1.0.jar cases/default.txt

## JavaDoc

    mvn javadoc:javadoc